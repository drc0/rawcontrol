#!/usr/bin/env pyhon
__author__ = "DRC <davide@licheni.net>"

import sys
import time

from conf import dev_info, backoff_time, APPNAME
from log import Logger
from control import loop
import dispatcher



log = Logger(APPNAME)
dispatcher = dispatcher.factory()

while True:
  try:
    loop(dev_info, dispatcher)
  except:
    e = sys.exc_info()[0]
    log.append(log.CRITICAL, 'Error in loop {}'.format(e))
    log.append(log.INFO, 'Sleeping for {}s'.format(backoff_time))
    time.sleep(backoff_time)
    if backoff_time < 100:
      backoff_time *= 2