import logging
import os

from pathlib import Path
from xdg import XDG_DATA_HOME
from logging import ERROR, INFO, WARNING, CRITICAL, DEBUG


class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Logger(metaclass=Singleton):
  
  ERROR = ERROR
  INFO = INFO
  WARNING = WARNING
  CRITICAL = CRITICAL
  DEBUG = DEBUG

  def __init__(self, app_name):

    log_dir = os.path.join(XDG_DATA_HOME, app_name)
    log_file = os.path.join(log_dir, 'log')

    Path(log_dir).mkdir(parents=True, exist_ok=True)
    self.logger = logging.getLogger(app_name)
    self.logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    self.logger.addHandler(fh)

  def append(self, level, string):
    self.logger.log(level, string)