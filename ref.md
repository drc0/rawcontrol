 VENDOR_ID       0xCB10
 PRODUCT_ID      0x1133
 DEVICE_VER      0x0100
 KEYBOARD_NAME = "BDN9 keypad";
 KEYBOARD_USAGE_ID =  0x3030;
 KEYBOARD_USAGE_PAGE = 0x2f32;

Udev rule for accessing the device from non root
SUBSYSTEMS=="usb", ATTRS{idVendor}=="cb10", ATTRS{idProduct}=="1133", MODE:="0666"
