import hid
import sys

from conf import backoff_time, APPNAME
from log import Logger

log = Logger(APPNAME)

def getpath(dev_info):
  path = ''
  for d in hid.enumerate():
    if d['product_id'] == dev_info['pid'] and d['vendor_id'] == dev_info['vid'] and d['interface_number'] == dev_info['interface_number']:# d['usage'] == usage_id and d['usage_page'] == usage_page: #d['interface_number'] == 1:
      path = d['path']
  return path


def write_to_dev(dev, cmd, msg_str):
  byte_str = chr(cmd) + msg_str + chr(0) * max(32 - len(msg_str), 0)
  try:
      num_bytes_written = dev.write(byte_str.encode())
  except IOError as e:
      log.append(log.CRITICAL, 'Error writing command: {}'.format(e))
      return None 

  return num_bytes_written

def read_from_dev(dev):
  try:
    cmd = dev.read(32)
    return cmd
  except IOError as e:
    log.append(log.CRITICAL, 'Error reading command: {}'.format(e))
    return None

def helo(dev):
  log.append(log.INFO, 'Sending helo')
  write_to_dev(dev, 0x01, "helo")
  _ = read_from_dev(dev)
  log.append(log.INFO, 'End helo')

def loop(dev_info, dispatcher):
  path = getpath(dev_info)
  if not path:
    return False
  backoff_time = 2
  with hid.Device(path=path) as h:
    helo(h)
    while True:
      cmd = read_from_dev(h)
      if cmd:
        dispatcher(cmd)