from notify import notify

from log import Logger
from conf import APPNAME


def dispatch(commands, cmd):
  log = Logger(APPNAME)
  log.append(log.INFO, 'Received cmd {}'.format(cmd[0]))
  func = getattr(commands, "cmd_%s" % cmd[0])
  if not func:
    log.append(log.INFO, 'No function for cmd {}'.format(cmd[0]))
    return False
  n = func()
  if n:
    notify(n)
  else:
    notify('Run cmd {}'.format(cmd[0]))
