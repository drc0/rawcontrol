import functools
from conf import dispatcher, commands
import importlib

def factory():
  dispatcher_f = importlib.import_module('dispatcher.{}'.format(dispatcher))
  commands_m = importlib.import_module('commands.{}'.format(commands))
  @functools.wraps(dispatcher_f)
  def dispatch(cmd):
    return dispatcher_f.dispatch(commands_m, cmd)
  return dispatch