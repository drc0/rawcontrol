APPNAME = 'rawcontrol'

dev_info = {
  'vid': 0xDC10,
  'pid': 0x0002,
  'interface_number': 1,
  'usage_page': 12082,
  'usage_id': 12336
}
backoff_time = 2

dispatcher = 'default'
commands = 'drc'
