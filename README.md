RAWCONTROL
==

Raw hid command dispatcher, reads raw hid commands and executes custom commands.

I'm using this with a bdn9 board with custom [firmware](https://bitbucket.org/drc0/qmk_firmware/src/drc/keyboards/drc/kp9/).

Installation
==

  ```
  git clone repo.git rawcontrol
  pip3 install rawcontrol/requirements.txt
  # personalize rawcontrol.dev_info with vid and cid for device
  # you can get them through lsusb
  python3 rawcontrol
  ```

In your terminal and in `$XDG_DATA_HOME/rawcontrol/log` you can see the state of the program.

Extend commands
==
You can switch between a set of commands by defining them in a common module and changing the
module name inside `conf.py`

  - create a `commands/{filename}.py`
  - inside `conf.py` change `commands` to `commands = '{filename}'

in your `commands/{filename}.py` define one function for command, they must respect the signature `cmd_{number}()`.

For example for:

  - command 1 (first switch click) `cmd_1()`
  - command 2 (second switch click) `cmd_2()`
  - ...


Extend dispatcher
==

You can change the default dispatcher:

  - create a `dispatcher/{filename}.py`
  - there define a function with signatre `fun(commands, cmd)`, and call cmd in commands module, see the default dispatcher for an implementation.
