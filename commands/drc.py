import subprocess
import sys

def cmd_1():
  subprocess.Popen(['konsole'])
  return 'Running konsole'

def cmd_2():
  subprocess.Popen(['pavucontrol'])
  return 'Running pavucontrol'

def cmd_3():
  subprocess.Popen(['sudo', 'hdparm','-y', '/dev/disk/by-id/ata-TOSHIBA_DT01ACA300_4680Y4NGS'])
  return 'Suspending /dev/disk/by-id/ata-TOSHIBA_DT01ACA300_4680Y4NGS'

def cmd_4():
  
  ports = [
    'analog-output-lineout',
    'analog-output-headphones'
  ]
  volumes = [
    ['Front 100%', 'Headphone 0%'],
    ['Front 0%', 'Headphone 100%'],
  ]
  titles = [
    'Speakers',
    'Headphones'
  ]
  checkcmd = 'pacmd list | grep "active port" | grep -o {};exit 0'.format(ports[0])
  currcmd = 0 if subprocess.check_output(checkcmd, stderr=subprocess.STDOUT, shell=True).decode('utf-8').strip() == ports[0] else 1
  currcmd = (currcmd + 1) % len(ports)
  subprocess.Popen('amixer -c0 sset "Auto-Mute Mode" Disabled', shell=True)
  subprocess.Popen('pacmd set-sink-port 0 ' +  ports[currcmd], shell=True)
  subprocess.Popen('amixer -c0 set ' + volumes[currcmd][0], shell=True)
  subprocess.Popen('amixer -c0 set ' + volumes[currcmd][1], shell=True)
  return 'Switching to ' + titles[currcmd]
