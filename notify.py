import notify2
import pathlib

from conf import *

def notify(msg):
  notify2.init(APPNAME)
  n = notify2.Notification(APPNAME, msg, str(pathlib.Path(__file__).parent.absolute()) + '/icon_small.png')
  n.show()